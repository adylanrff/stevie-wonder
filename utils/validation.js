export const validatePhoneNumber = (phoneNumber) => {
    if (!phoneNumber) {
        return false
    }

    const CORRECT_NUMBER_LENGTH = 12
    let isNum = /^\d+$/.test(phoneNumber);
    return CORRECT_NUMBER_LENGTH === phoneNumber.length && isNum
}

export const validateNominal = (nominal) => {
    if (!nominal) {
        return false
    }

    let isNum = /^^[^0]\d+$/.test(nominal);
    return nominal.length > 4 && isNum
}