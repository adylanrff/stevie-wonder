import PDAMDistrict from "../screens/transactions/PDAMScreen/PDAMDistrict";

const PDAM_HELP = [{
    header: "Cara Pembayaran Tagihan Air",
    content: `
1. Masukkan nomor meter Anda, jika Anda tidak mengetahuinya dapat mengetuk tombol info di sebelah kanan kotak edit nomor meter

2. Setelah muncul total tagihan, pilih ‘Lanjutkan Pembayaran’. Jika tombol ‘Lanjutkan Pembayaran’ tidak dapat ditekan, maka saldo Anda tidak cukup atau nomor pelanggan yang Anda masukkan tidak terdaftar

3. Periksa kembali pembayaran Anda, pilih ‘Ubah’ jika ingin mengubah nomor meter, dan pilih ‘Konfirmasi’ untuk membayar

4. Setelah memilih ‘Konfirmasi’, layar akan memberitahukan status keberhasilan transaksi
    `,
}]

const TAGIHAN_LISTRIK_HELP = [
    {
        header: "Cara Pembelian Token Listrik",
        content: `
1. Masukkan nomor meter Anda, jika Anda tidak mengetahuinya dapat mengetuk tombol info di sebelah kanan kotak edit nomor meter. Jika nominal voucher belum muncul, maka masukan nomor pelanggan Anda masih salah

2. Setelah pilihan nominal voucher muncul, Anda dapat memilih voucher listrik yang Anda inginkan

3. Setelah memilih voucher listrik, periksa kembali pembelian Anda, pilih ‘Ubah’ jika ingin mengubah pembelian, dan pilih ‘Konfirmasi’ untuk membayar. Jika tombol ‘Konfirmasi’ tidak dapat ditekan, maka saldo Anda tidak cukup

4. Setelah memilih ‘Konfirmasi’, layar akan memberitahukan status keberhasilan transaksi
`
    },
    {
    header: "Cara Pembayaran Tagihan PLN",
    content: `
1. Masukkan nomor meter Anda, jika Anda tidak mengetahuinya dapat mengetuk tombol info di sebelah kanan kotak edit nomor meter

2. Setelah muncul total tagihan, pilih ‘Lanjutkan Pembayaran’. Jika tombol ‘Lanjutkan Pembayaran’ tidak dapat ditekan, maka saldo Anda tidak cukup atau nomor pelanggan yang Anda masukkan tidak terdaftar

3. Periksa kembali pembayaran Anda, pilih ‘Ubah’ jika ingin mengubah nomor meter, dan pilih ‘Konfirmasi’ untuk membayar

4. Setelah memilih ‘Konfirmasi’, layar akan memberitahukan status keberhasilan transaksi
`
    }
]

const PULSA_HELP = [
    {
    header: "Cara Pembelian Pulsa",
    content: `
1. Masukkan nomor ponsel Anda, Anda juga dapat mencarinya dari daftar kontak Anda atau memilih nomor yang telah Anda masukkan sebelumnya. Jika pilihan pulsa belum muncul, maka masukan nomor pelanggan Anda masih salah

2. Setelah pilihan nominal pulsa muncul sesuai dengan provider Anda, Anda dapat memilih voucher pulsa yang Anda inginkan

3. Setelah memilih voucher pulsa, periksa kembali pembelian Anda, pilih ‘Ubah’ jika ingin mengubah pembelian, dan pilih ‘Konfirmasi’ untuk membayar. Jika tombol ‘Konfirmasi’ tidak dapat ditekan, maka saldo Anda tidak cukup

4. Setelah memilih ‘Konfirmasi’, layar akan memberitahukan status keberhasilan transaksi
`
    },
    {
    header: "Cara Paket Data",
    content: `
1. Masukkan nomor ponsel Anda, Anda juga dapat mencarinya dari daftar kontak Anda atau memilih nomor yang telah Anda masukkan sebelumnya. Jika pilihan paket data belum muncul, maka masukan nomor pelanggan Anda masih salah

2. Setelah pilihan paket data muncul sesuai dengan provider Anda, Anda dapat memilih paket data yang Anda inginkan

3. Setelah memilih paket data, periksa kembali pembelian Anda, pilih ‘Ubah’ jika ingin mengubah pembelian, dan pilih ‘Konfirmasi’ untuk membayar. Jika tombol ‘Konfirmasi’ tidak dapat ditekan, maka saldo Anda tidak cukup

4. Setelah memilih ‘Konfirmasi’, layar akan memberitahukan status keberhasilan transaksi Anda`
    },
]

const TOPUP_HELP = [
    {
        header: "Cara Pengisian Saldo dengan Kartu Kredit",
        content: `
1. Masukkan nominal top-up, dan informasi kartu seperti nomor kartu, masa berlaku, dan CVV

2. Pilih tombol ‘Lanjutkan Top-up’. Jika tombol ‘Lanjutkan Top-up’ tidak dapat ditekan, maka kartu Anda tidak terdaftar

3. Periksa kembali detail pengisian saldo Anda, pilih ‘Ubah’ jika ingin menggubah informais kartu atau nominal top-up dan pilih ‘Top-up’ jika ingin melakukan top-up

4. Setelah memilih ‘Top-up’, layar akan memberitahukan status keberhasilan transaksi
`
    },
    {
        header: `Cara Pengisian Saldo dengan Kartu Debit`,
        content: `
1. Masukkan nominal top-up, dan informasi kartu seperti nomor kartu, masa berlaku, dan CVV

2. Pilih tombol ‘Lanjutkan Top-up’. Jika tombol ‘Lanjutkan Top-up’ tidak dapat ditekan, maka kartu Anda tidak terdaftar

3. Periksa kembali detail pengisian saldo Anda, pilih ‘Ubah’ jika ingin menggubah informais kartu atau nominal top-up dan pilih ‘Top-up’ jika ingin melakukan top-up

4. Setelah memilih ‘Top-up’, layar akan memberitahukan status keberhasilan transaksi
`
    },
    {
        header: "Cara Pengisian Saldo melalui Mobile Banking",
        content: `
1. Pilih salah satu aplikasi mobile banking yang Anda gunakan

2. Ikuti instruksi
`
    },
    {
        header: "Cara Pengisian Saldo melalui ATM",
        content: `
1. Pilih salah satu Bank yang Anda gunakan

2. Ikuti instruksi
`
    },
]

const TRANSFER_HELP = [
    { 
        header: "Cara Transfer ke Sesama E-wallet",
        content: `
1. Masukkan nomor ponsel

2. Masukkan jumlah nominal yang ingin Anda krimkan

3. Pilih ‘Lanjutkan Transfer’, jika tombol tersebut tidak dapat dipilih, maka masih terdapat kesalahan masukan, seperti nomor ponsel yang salah atau saldo Anda tidak cukup

4. Periksa kembali rincian transaksi transfer Anda, jika ingin mengubahnya pilih ‘Ubah’ dan jika ingin melanjutkan transfer pilih ‘Konfirmasi’

5. Setelah memilih ‘Konfirmasi’, layar akan memberitahukan status keberhasilan transaksi
`
    }
]

export const getHelpString = (route) => {
    console.log(route.name)
    switch (route.name) {
        case "PDAM":
            return PDAM_HELP
        case "Listrik":
            return TAGIHAN_LISTRIK_HELP
        case "Pulsa":
            return PULSA_HELP
        case "TopUp": 
            return TOPUP_HELP
        case "Transfer": 
            return TRANSFER_HELP
        case "Riwayat": 
            return TRANSFER_HELP
        default:
            break;
    }
} 