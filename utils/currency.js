export const convertIntToIdrString = (number) => {
    const formatter = Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR'
    })
    const formattedBalance = formatter.format(number)
    return formattedBalance        
}
