export const transformGridItem = (items, numColumns) => {
    if (!items) {
        return
    }

    const numItemsToAdd = items.length % numColumns > 0 ? numColumns - (items.length % numColumns) : 0
    const addedItems = items.slice(0)
    for (let i = 0; i < numItemsToAdd; i++) {
        addedItems.push({ isPlaceholder: true })
    }

    return addedItems
} 

