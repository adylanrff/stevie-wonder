import { atom, selector } from 'recoil'

export const PDAMDistrictState = atom({
    key: "PDAMDistrictState",
    default: [
        {
            city: "Madiun",
            province: "Jawa Timur"
        },
        {
            city: "Surabaya",
            province: "Jawa Timur"
        },
        {
            city: "Pasuruan",
            province: "Jawa Timur"
        },
        {
            city: "Madiun",
            province: "Jawa Timur"
        },
        {
            city: "Surabaya",
            province: "Jawa Timur"
        },
        {
            city: "Pasuruan",
            province: "Jawa Timur"
        },
    ]
})

export const latestPDAMDistrictState = selector({
    key: "latestPDAMDistrictState",
    get: ({get}) => {
        const pdamDistrictState = get(PDAMDistrictState)
        return pdamDistrictState[0]
    }
})