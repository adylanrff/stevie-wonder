import { atom, selector } from 'recoil';

export const pulsaOptionsState = atom({
    key: "pulsaOptionsState",
    default: [
        {
            title: "5000",
            price: 5000
        },
        {
            title: "5000",
            price: 5000
        },
        {
            title: "5000",
            price: 5000
        },
        {
            title: "5000",
            price: 5000
        },
        {
            title: "5000",
            price: 5000
        },
    ]
})

export const packetOptionState = atom({
    key: "packetOptionsState",
    default: [
        {
            title: "Paket Internet 30K 1 Bulan",
            subtitle: "Kuota utama dan 1 GB untuk sosial. Berlaku selama 30 hari",
            price: 5000
        },
        {
            title: "Paket Internet 30K 1 Bulan",
            subtitle: "Kuota utama dan 1 GB untuk sosial. Berlaku selama 30 hari",
            price: 5000
        },
        {
            title: "Paket Internet 30K 1 Bulan",
            subtitle: "Kuota utama dan 1 GB untuk sosial. Berlaku selama 30 hari",
            price: 5000
        },
        {
            title: "Paket Internet 30K 1 Bulan",
            subtitle: "Kuota utama dan 1 GB untuk sosial. Berlaku selama 30 hari",
            price: 5000
        },
        {
            title: "Paket Internet 30K 1 Bulan",
            subtitle: "Kuota utama dan 1 GB untuk sosial. Berlaku selama 30 hari",
            price: 5000
        },
    ]
})

export const electricBillState = atom({
    key: "electricBillState",
    default: {
        "087838888671": 500000
    }
})

export const PDAMBillState = atom({
    key: "PDAMBillstate",
    default: {
        "087838888671": 500000
    }
})
