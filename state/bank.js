import { atom, selector } from 'recoil'

export const bankState = atom({
    key: 'bankState',
    default: [
        {
            name: "Bank BNI",
            guide: [
                "Login ke akun BNI Mobile yang Anda miliki",
                "Pilih menu ‘Transfer’",
                "Pilih menu ‘Virtual Account’",
                `Masukkan ‘13516’ + nomor ponsel Anda

Contoh: 13516081234567890`,
                `Masukkan nominal top-up yang Anda inginkan`,
                `Ikuti instruksi pada mesin ATM hingga selesai`,
            ]
        },
        {
            name: "Bank Mandiri",
            guide: [
                "Login ke akun Bank Mandiri yang Anda miliki",
                "Pilih menu ‘Transfer’",
                "Pilih menu ‘Virtual Account’",
                `Masukkan ‘13516’ + nomor ponsel Anda

Contoh: 13516081234567890`,
                `Masukkan nominal top-up yang Anda inginkan`,
                `Ikuti instruksi pada mesin ATM hingga selesai`,
            ]

        },
        {
            name: "Bank BTN",
            guide: [
                "Login ke akun Bank BTN yang Anda miliki",
                "Pilih menu ‘Transfer’",
                "Pilih menu ‘Virtual Account’",
                `Masukkan ‘13516’ + nomor ponsel Anda

Contoh: 13516081234567890`,
                `Masukkan nominal top-up yang Anda inginkan`,
                `Ikuti instruksi pada mesin ATM hingga selesai`,
            ]

        },
    ]
})

export const latestBankState = selector({
    key: 'latestBankState',
    get: ({get}) => {
        const banks = get(bankState)
        return banks.slice(0,1)
    }
})