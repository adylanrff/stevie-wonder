import { atom, selector } from 'recoil'

export const shouldShowOtherModalState = atom({
    key: "shouldShowOtherModalState",
    default: false
})