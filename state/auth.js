import { atom, selector } from "recoil";


export const userState = atom({
  key: "userState",
  default: {
    userName: "Azka Nabilah Mumtaz",
    isAuthenticated: false,
    balance: 1000000,
    expense: 500000,
    phoneNumber: "081234561789"
  }
})

export const isAuthenticatedState = selector({
  key: "isAuthenticatedState",
  get: ({get}) => {
    const user = get(userState)
    return user.isAuthenticated;
  }
});

export const userBalanceState = selector({
  key: "userBalanceState",
  get: ({get}) => {
    const user = get(userState)
    return user.balance
  }
})

export const userNameState = selector({
  key: "userNameState",
  get: ({get}) => {
    const user = get(userState)
    return user.userName
  }
})

export const userExpenseState = selector({
  key: "userExpenseState",
  get: ({get}) => {
    const user = get(userState)
    return user.expense
  }
})

export const userPhoneNumberState = selector({
  key: "userPhoneNumberState",
  get: ({get}) => {
    const user = get(userState)
    return user.phoneNumber
  }
})