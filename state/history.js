import { atom, selector } from 'recoil'
import { convertIntToIdrString } from '../utils/currency'

export const allHistoryState = atom({
    key: "allHistoryState",
    default: [
        {
            type: "pembayaran",
            title: "Pembayaran Air PDAM",
            subtitle: `Berkurang ${convertIntToIdrString(2000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "transfer",
            title: "Transfer ke ody",
            subtitle: `Berkurang ${convertIntToIdrString(390000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "topup",
            title: "Top Up debit",
            subtitle: `Berkurang ${convertIntToIdrString(1000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "pembayaran",
            title: "Pembayaran Air PDAM",
            subtitle: `Berkurang ${convertIntToIdrString(2000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "transfer",
            title: "Transfer ke ody",
            subtitle: `Berkurang ${convertIntToIdrString(390000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "topup",
            title: "Top Up debit",
            subtitle: `Berkurang ${convertIntToIdrString(1000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "pembayaran",
            title: "Pembayaran Air PDAM",
            subtitle: `Berkurang ${convertIntToIdrString(2000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "transfer",
            title: "Transfer ke ody",
            subtitle: `Berkurang ${convertIntToIdrString(390000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "topup",
            title: "Top Up debit",
            subtitle: `Berkurang ${convertIntToIdrString(1000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "pembayaran",
            title: "Pembayaran Air PDAM",
            subtitle: `Berkurang ${convertIntToIdrString(2000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "transfer",
            title: "Transfer ke ody",
            subtitle: `Berkurang ${convertIntToIdrString(390000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "topup",
            title: "Top Up debit",
            subtitle: `Berkurang ${convertIntToIdrString(1000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "pembayaran",
            title: "Pembayaran Air PDAM",
            subtitle: `Berkurang ${convertIntToIdrString(2000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "transfer",
            title: "Transfer ke ody",
            subtitle: `Berkurang ${convertIntToIdrString(390000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "topup",
            title: "Top Up debit",
            subtitle: `Berkurang ${convertIntToIdrString(1000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "pembayaran",
            title: "Pembayaran Air PDAM",
            subtitle: `Berkurang ${convertIntToIdrString(2000000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "transfer",
            title: "Transfer ke ody",
            subtitle: `Berkurang ${convertIntToIdrString(390000)}`,
            date: "30 Juli 2020"
        },
        {
            type: "topup",
            title: "Top Up debit",
            subtitle: `Berkurang ${convertIntToIdrString(1000000)}`,
            date: "30 Juli 2020"
        },
    ]
})

export const transferHistoryState = selector({
    key: "transferHistoryState",
    get: ({get}) => {
        const allHistory = get(allHistoryState);
        return allHistory.filter((history) => history.type === "transfer")
    }
})

export const topUpHistoryState = selector({
    key: "topUpHistoryState",
    get: ({get}) => {
        const allHistory = get(allHistoryState);
        return allHistory.filter((history) => history.type === "topup")
    }
})

export const paymentHistoryState = selector({
    key: "paymentHistoryState",
    get: ({get}) => {
        const allHistory = get(allHistoryState);
        return allHistory.filter((history) => history.type === "pembayaran")
    }
})