import { atom, selector } from 'recoil';

export const transactionState = atom({
    key: "transactionState",
    default: [
        {
            title: "Pembayaran Air PDAM",
            screenName: "PDAM",
            order: 1,
        },
        {
            title: "Pembelian Pulsa/Data",
            screenName: "Pulsa",
            order: 2,
        },
        {
            title: "Pembelian Listrik PLN",
            screenName: "Listrik",
            order: 3,
        },
        {
            title: "Pembayaran Lainnya",
            screenName: "Other",
            order: 4,
        },
    ]
})

export const topUpTransactionState = atom({
    key: "topUpTransactionState",
    default: [
        {
            title: "Kartu Kredit",
            screenName: "TopUpCredit",
            order: 1,
        },
        {
            title: "Debit",
            screenName: "TopUpDebit",
            order: 2,
        },
        {
            title: "Mobile Banking",
            screenName: "TopUpMB",
            order: 3,
        },
        {
            title: "ATM",
            screenName: "TopUpATM",
            order: 4,
        },
    ]
})

export const numberHistoryState = atom({
    key: "numberHistoryState",
    default: [
        { 
            number: "081237192380"
        },
        { 
            number: "081237192312"
        },
        { 
            number: "081237192823"
        },
    ]
})

export const latestTransactionState = selector({
    key: "latestTransactionState",
    get: ({get}) => {
        const transaction = get(transactionState)
        console.log(transaction)
        return transaction.slice(0, 1)
    }
})

export const otherTransactionState = atom({
    key: "otherTransactionState",
    default: {
        merchant: "McDonalds",
        price: 999000,
        fee: 1000,
    }
})