import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/login';
import FingerprintScreen from '../screens/fingerprint';

const Stack = createStackNavigator()

const AuthNavigator = () => {
    return (
        <Stack.Navigator    
            initialRouteName="Fingerprint" 
            screenOptions={{ headerShown: false }}
        >
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Fingerprint" component={FingerprintScreen} />
        </Stack.Navigator>
    )
}

export default AuthNavigator;