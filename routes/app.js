import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/home';
import SettingsScreen from '../screens/settings';
import PulsaScreen from '../screens/transactions/PulsaScreen';
import ListrikScreen from '../screens/transactions/ListrikScreen';
import PDAMScreen from '../screens/transactions/PDAMScreen';
import OtherScreen from '../screens/transactions/OtherScreen';
import TransactionDetailScreen from '../screens/transactions/TransactionDetailScreen';
import TransactionSuccessScreen from '../screens/transactions/TransactionSuccessScreen';
import TransactionFailedScreen from '../screens/transactions/TransactionFailedScreen';
import TopUpScreen from '../screens/transactions/TopUpScreen';
import TransferScreen from '../screens/transactions/TransferScreen';
import HistoryScreen from '../screens/transactions/HistoryScreen';
import HelpScreen from '../screens/help';
import ConfirmationScreen from '../screens/transactions/ConfirmationScreen';
import FingerprintScreen from '../screens/fingerprint';
import TopUpGuideScreen from '../screens/transactions/TopUpScreen/TopUpGuideScreen';

const Stack = createStackNavigator()

const AppNavigator = () => {
    return (
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="Pulsa" component={PulsaScreen} />
            <Stack.Screen name="Listrik" component={ListrikScreen} />
            <Stack.Screen name="PDAM" component={PDAMScreen} />
            <Stack.Screen name="Other" component={OtherScreen} />
            <Stack.Screen name="TopUp" component={TopUpScreen} />
            <Stack.Screen name="TopUpGuide" component={TopUpGuideScreen} />
            <Stack.Screen name="Transfer" component={TransferScreen} />
            <Stack.Screen name="History" component={HistoryScreen} />
            <Stack.Screen name="Transaction" component={TransactionDetailScreen} />
            <Stack.Screen name="Confirmation" component={ConfirmationScreen} options={{ headerShown: false}}/>
            <Stack.Screen name="Fingerprint" component={FingerprintScreen} options={{ headerShown: false}}/>
            <Stack.Screen name="TransactionSuccess" component={TransactionSuccessScreen} options={{ headerShown: false}}/>
            <Stack.Screen name="TransactionFailed" component={TransactionFailedScreen} options={{ headerShown: false}}/>
            <Stack.Screen name="Settings" component={SettingsScreen} />
            <Stack.Screen name="Help" component={HelpScreen} />
        </Stack.Navigator>
    )
}

export default AppNavigator;