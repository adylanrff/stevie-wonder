import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AppRoute from './app'
import AuthRoute from './auth'
import { useRecoilState } from 'recoil';
import { isAuthenticatedState } from '../state/auth';
import { createStackNavigator } from '@react-navigation/stack';



const CoreNavigator = () => {

    const Stack = createStackNavigator()

    const [isAuthenticated, setIsAuthenticated] = useRecoilState(isAuthenticatedState)
    
    useEffect(() => {
        console.log(isAuthenticated)
    }, [isAuthenticated])

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Auth" screenOptions={{headerShown: false}}>
            { isAuthenticated ? <Stack.Screen name="App" component={AppRoute} /> : <Stack.Screen name="Auth" component={AuthRoute} />  }
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default CoreNavigator;