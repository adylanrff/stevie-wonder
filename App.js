import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { RecoilRoot } from 'recoil';
import CoreNavigator from './routes/core';

const Stack = createStackNavigator();

function App() {
  return (
    <RecoilRoot>
      <CoreNavigator />
    </RecoilRoot>
  );
}

export default App;