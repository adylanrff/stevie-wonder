import React from 'react'
import { StyleSheet, Text } from 'react-native'


const HeaderTitle = ({ title }) => (
    <Text style={styles.headerText}>{title}</Text>
)

const styles = StyleSheet.create({
    headerText: {
        textAlign: 'center'
    }
})

export default HeaderTitle;