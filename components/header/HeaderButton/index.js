import React from 'react'
import { Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const HeaderButton = ({ iconName, onPress, accessibilityLabel }) => (
    <Icon.Button 
        name={iconName} 
        accessibilityLabel={accessibilityLabel}
        onPress={onPress}
        color="black"
        backgroundColor="white"
        style={styles.button}
    />
)

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 8
    }
})

export default HeaderButton;