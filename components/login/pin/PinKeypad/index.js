import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList, Dimensions } from 'react-native';
import KeypadButton from './KeypadButton'

const PinKeypad = (props) =>  {
    const numColumns = 3;
    
    const { keypadContents, onKeyPress, onDeleteKeyPress, onFingerPrintKeyPress } = props;

    
    const renderItem = ({item, index}) => {
        let onPressAction;
        switch (item.key) {
            case '<<':
                onPressAction = onDeleteKeyPress
                break;
            case 'S': 
                onPressAction = onFingerPrintKeyPress
                break;
            default:
                onPressAction = onKeyPress
                break;
        }

        return <KeypadButton content={item.key} onPress={() => onPressAction(item.key)} />
    }
 
    return (
        <FlatList
            style={styles.container}
            horizontal={false}
            keyExtractor={(item, index) => index.toString()}
            data={keypadContents}
            numColumns={numColumns}
            renderItem={renderItem}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        marginTop: Dimensions.get('screen').width / 4,
    }
}) 

export default PinKeypad;