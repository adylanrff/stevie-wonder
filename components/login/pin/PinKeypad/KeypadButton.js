import * as React from 'react';
import { StyleSheet, Button, Text, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const KeypadButton = ({ content, onPress }) => {

    return (
        <TouchableOpacity 
            style={styles.container} 
            onPress={onPress}
        >
            <Text style={styles.text} >{content}</Text> 
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        padding: 10,
        width: Dimensions.get('screen').width / 3,
        height: Dimensions.get('screen').height / 6,
        justifyContent: 'center'
    },
    text: {
        color: "black",
        textAlign: "center",
        fontSize: 24,
    }
})

export default KeypadButton;