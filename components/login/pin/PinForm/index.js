import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import PinKeypad from '../PinKeypad';
import PinView from '../PinView';


const PinForm = ({ maxPinLength, keypadContents, onSubmit, onFingerprintPress }) => {
    const [pressedPins, setPressedPins] = useState([])
  
    const onKeypadPress = (keyPressed) => {
      if (pressedPins.length < maxPinLength ){ 
        setPressedPins([...pressedPins, keyPressed])
      } 
    }
  
    const onDeleteKeypadPress = () => {
      setPressedPins(pressedPins.slice(0,-1))
    }

    const onFingerPrintKeyPress = () => {
        onFingerprintPress()
    }

    useEffect(() => {
        if (pressedPins.length === maxPinLength) {
            onSubmit()
        }
    }, [pressedPins])
  
    return (
        <View style={styles.container}>
            <PinView 
                maxPinLength={maxPinLength} 
                pressedLength={pressedPins.length}
            />
            <PinKeypad 
                keypadContents={keypadContents}
                onFingerPrintKeyPress={onFingerPrintKeyPress}
                onKeyPress={onKeypadPress} 
                onDeleteKeyPress={onDeleteKeypadPress}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default PinForm