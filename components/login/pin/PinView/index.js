import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';

const PinView = (props) => {
    const { style, maxPinLength, pressedLength } = props
    const textToRender = [...Array(maxPinLength)].map((e,i) => i < pressedLength ? "●" : "◯").join('     ')
    return (
        <View style={{...style, ...styles.container}}>
            <Text style={styles.headerText}>Masukkan nomor PIN Anda</Text>
            <Text style={{...style, ...styles.password}}>{textToRender}</Text>
            <Text style={styles.forgotPasswordText}>Lupa PIN? klik di sini</Text>
        </View>
)}

const styles = StyleSheet.create({
    container: {
        marginTop: Dimensions.get('screen').height / 24,
        marginBottom: 20,
        padding: 25,
        flex: 2,
    },
    headerText: {
        marginBottom: 32,
        fontSize: 16,
        textAlign: 'center'
    },
    forgotPasswordText: {
        marginTop: 32,
        textAlign: 'center',
    },
    password: {
        fontSize: 16,
        textAlign: 'center'
    }
})

export default PinView;