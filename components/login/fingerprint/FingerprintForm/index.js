import React from 'react'
import { View, Text, StyleSheet, Dimensions, Button } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const FingerprintForm = ({ onSubmit, onBack }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>E-wallet</Text>
            <View style={styles.prompt}>
                <TouchableOpacity style={styles.mockImage} onPress={onSubmit}/>
                <Text style={styles.promptText}>Letakkan jari anda pada sensor sidik jari perangkat anda</Text>    
            </View>
            <Button onPress={onBack} title={"Masuk dengan PIN"} color="lightgrey"/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        marginVertical: 64,
    },
    prompt: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mockImage: {
        width: Dimensions.get('screen').width / 2,
        height: Dimensions.get('screen').height / 4,
        borderStyle: 'solid',
        backgroundColor: 'lightgrey',
        marginBottom: 24,
    },
    promptText: {
        fontSize: 16,
        marginHorizontal: 24,
        textAlign: "center"
    },
    headerText: {
        fontSize: 32,
        textAlign: 'center'
    },
})


export default FingerprintForm;