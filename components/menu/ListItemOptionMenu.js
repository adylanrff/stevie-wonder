import React from 'react'
import { Text, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ListItemOptionMenu = ({item, onPress}) => (
    <TouchableOpacity style={styles.container} onPress={onPress}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.subtitle}>{item.subtitle}</Text>
        <Text style={styles.price}> {item.price} </Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 15,
        paddingHorizontal: 15,
        marginVertical: 5,
        alignItems: 'flex-start',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    subtitle: {
        fontSize: 13,
    },
    price: {
        fontSize: 15,
        marginTop: 10,
        fontWeight: 'bold'
    }
})

export default ListItemOptionMenu;