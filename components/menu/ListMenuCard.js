import React from 'react';
import { Image, StyleSheet, View, Text, Dimensions } from 'react-native';
import placeholderImg from '../../assets/image/placeholder.jpg'
import { TouchableOpacity } from 'react-native-gesture-handler';

const ListMenuCard = ({ img, text, subtitle, textColor, onPress }) => {
    const textColorStyle = { color: textColor }
    
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchable} onPress={onPress}>
                <Image source={img || placeholderImg} style={styles.image}/>
                <View style={styles.textSection}>
                    <Text style={{...styles.text, ...textColorStyle}}>{text}</Text>
                    {subtitle && <Text style={styles.subtitle}>{subtitle}</Text>}
                </View>
            </TouchableOpacity>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        marginVertical: 5,
        paddingHorizontal: 15,
        paddingVertical: 20,
        flex: 1,
        backgroundColor:'#FAFAFA',
    },
    touchable: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    image: {
        width: 64,
        height: 64,
        flex: 1,
    },
    textSection: {
        flex: 3,
        marginLeft: 20,
        justifyContent: 'space-between'
    },
    text: {
        flex: 1,
    },
    subtitle: {
        flex: 1,
    },
})


export default ListMenuCard;