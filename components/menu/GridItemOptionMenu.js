import React from 'react'
import { Text, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { convertIntToIdrString } from '../../utils/currency';

const GridItemOptionMenu = ({item, onPress}) => (
    <TouchableOpacity style={styles.container} onPress={onPress}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.subtitle}>{convertIntToIdrString(item.price )}</Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingHorizontal: 10,
        paddingVertical: 15,
        marginVertical: 5,
        marginHorizontal: 10,
        width: Dimensions.get('screen').width * 3/8,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 24,
        marginBottom: 10,
    },
    subtitle: {
        fontSize: 14,
    }
})

export default GridItemOptionMenu;