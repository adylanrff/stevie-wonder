import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import MenuModalButton from './MenuModalButton';
import { convertIntToIdrString } from '../../../utils/currency';


const MenuModal = ({ navigation, expenses }) => {
    const handleTopUpButton = () => {
        navigation.navigate('TopUp')
    }
    const handleTransferButton = () => {
        navigation.navigate('Transfer')
    }
    const handleHistoryButton = () => {
        navigation.navigate('History')
    }
    return (
        <View style={styles.container}>
            <View style={styles.expensesSection}>
                <Text style={styles.expenseHeaderText}>Pengeluaran bulan ini</Text>
                <Text style={styles.expenseContentText}>{convertIntToIdrString(expenses)}</Text>
            </View>
            <View style={styles.menuSection}>
                <MenuModalButton 
                    iconName="plus-circle"
                    text="Top up"
                    onPress={handleTopUpButton}
                />
                <MenuModalButton 
                    iconName="exchange"
                    text="Transfer"
                    onPress={handleTransferButton}
                />
                <MenuModalButton
                    iconName="history"
                    text="Riwayat"
                    onPress={handleHistoryButton}
                />
            </View>
        </View>
)}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingVertical: 15,
        width: Dimensions.get('screen').width * 2/3,
        height: Dimensions.get('screen').height / 5,
        marginVertical: 20,
        justifyContent: 'space-between'
    },
    expensesSection: {
        alignItems: 'center',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        paddingBottom: 20,
    },
    expenseHeaderText: {

    },
    expenseContentText: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    menuSection: {
        justifyContent: 'space-evenly',
        flexDirection: 'row'
    }
})

export default MenuModal;