import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome'
import { StyleSheet, Text, View } from 'react-native'

const MenuModalButton = ({ iconName, text, onPress }) => (
    <TouchableOpacity style={styles.container} onPress={onPress}>
        <Icon name={iconName} style={styles.icon}/>
        <Text>{text}</Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    icon: {
        fontSize: 36,
    }
})

export default MenuModalButton;