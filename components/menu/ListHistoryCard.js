import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native-gesture-handler'

const ListHistoryCard = ({ title, subtitle, date, onPress }) => {

    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <View style={styles.textSection}>
                <View style={styles.info}>
                    <Text style={styles.titleText}>{title}</Text>
                    <Text style={styles.subtitleText}>{subtitle}</Text>
                </View>
                <View style={styles.date}>
                    <Text style={styles.dateText}>{date}</Text>
                </View>
            </View>
            <View style={styles.iconSection}>
                <Icon style={styles.icon} name="arrow-circle-o-right"/>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingHorizontal: 15,
        paddingVertical: 15,
        marginVertical: 10,
        flexDirection: 'row',
        minHeight: 120,
    },
    textSection: {
        justifyContent: 'space-between',
        flex: 12,
    },
    titleText: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    subtitleText: {
        fontSize: 12,
    },
    date: {
    },
    dateText: {
        fontSize: 12,  
    },
    info: {
    },
    iconSection: {
        flex: 1,
        justifyContent: 'center'
    },
    icon: {
        fontSize: 24,
    }
})

export default ListHistoryCard