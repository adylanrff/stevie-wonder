import React from 'react';
import { Image, StyleSheet, View, Text, Dimensions } from 'react-native';
import placeholderImg from '../../assets/image/placeholder.jpg'
import { TouchableOpacity } from 'react-native-gesture-handler';

const GridMenuCard = ({ img, text, textColor, onPress }) => {
    const textColorStyle = { color: textColor }
    
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchable} onPress={onPress}>
                <Image source={img || placeholderImg} style={styles.image}/>
                <Text style={{...styles.text, ...textColorStyle}}>{text}</Text>
            </TouchableOpacity>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        padding: 10,
        backgroundColor:'#FAFAFA',
        justifyContent: 'center',
        alignItems: 'center'
    },
    touchable: {
        alignItems: 'center',
    },
    image: {
        width: 64,
        height: 64,
        alignSelf: 'center'
    },
    text: {
        textAlign: 'center'
    }
})


export default GridMenuCard;