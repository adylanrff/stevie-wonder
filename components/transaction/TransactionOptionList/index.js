import React from 'react'
import { FlatList, View, Text, StyleSheet, Dimensions } from 'react-native'

const TransactionOptionList = ({ headerTitle, headerSubtitle, optionData, numColumns, renderItem }) => (
    <View style={styles.container}>
        <View style={styles.textContainer}>
            <Text style={styles.headerText}>{headerTitle}</Text>
            <Text style={styles.subtitleText}>{headerSubtitle}</Text>
        </View>
        <FlatList
            data={optionData}
            key={numColumns}
            keyExtractor={(item, index) => index.toString()}
            numColumns={numColumns}
            contentContainerStyle={styles.flexGrow}
            renderItem={renderItem}
        />
    </View>
)

const styles = StyleSheet.create({
    container: {
        marginTop: 24,
        justifyContent: 'space-between',
        width: Dimensions.get('screen').width * 7/8
    },
    listContainer: {
        flexGrow: 1,
    },
    textContainer: {
        marginBottom: 20,
        marginHorizontal: 5,
    },
    headerText: {
        fontSize: 20,
    },
    subtitleText: {
        fontSize: 12,
    },
    optionList: {
        alignItems: 'center'
    }
})

export default TransactionOptionList;