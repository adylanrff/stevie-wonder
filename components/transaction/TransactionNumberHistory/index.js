import React from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import { useRecoilState } from 'recoil'
import { numberHistoryState } from '../../../state/transaction'

const TransactionNumberHistory = ({ headerText, numberHistory, onNumberPress }) => {

    const renderNumberHistory = ({item}) => (
        <TouchableOpacity style={styles.numbers} onPress={() => onNumberPress(item.number)}>
            <Text style={styles.numberText}>{item.number}</Text>
        </TouchableOpacity>
    )

    return (
        <View style={styles.container}>
            <Text style={styles.headerText} >{headerText}</Text>
            <FlatList 
                data={numberHistory}
                renderItem={renderNumberHistory}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 32,
    },
    numbers: {
        marginVertical: 10,
        paddingHorizontal: 5,
        paddingVertical: 15,
        backgroundColor: 'white',
        width: Dimensions.get('screen').width * 5/6
    },
    numberText: {
        textAlign: 'center',
        fontSize: 18,
    },
    headerText: {
        textAlign: 'left',
        marginBottom: 20,
        fontWeight: 'bold'
    }

})

export default TransactionNumberHistory;