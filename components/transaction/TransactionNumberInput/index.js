import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, TextInput, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native-gesture-handler'

const TransactionNumberInput = ({ 
    headerText, 
    iconName, 
    iconOnPress, 
    onCorrectInput, 
    onFalseInput,
    number,
    setNumber,
    validation
}) => {
    const [isCorrectNumber, setIsCorrectNumber] = useState(false)

    useEffect(() => {
        if (!validation || validation(number)) {
            setIsCorrectNumber(true)
            if (onCorrectInput){
                onCorrectInput()
            }
        } else {
            setIsCorrectNumber(false)
            if (onFalseInput){
                onFalseInput()
            }
        }
    }, [number])
    
    return (
        <View style={styles.root}>
            <View style={styles.container}>
                <Text style={styles.headerText}>{headerText}</Text>
                <View style={styles.inputContainer}>
                    <TextInput 
                        style={styles.textInput} 
                        onChangeText={setNumber}
                        maxLength={20}
                        value={number}
                        keyboardType="number-pad"
                    />
                    {iconName && 
                        <TouchableOpacity style={styles.icon} onPress={iconOnPress}>
                            <Icon style={styles.inputIcon} name={iconName} color="grey" />
                        </TouchableOpacity>
                    }
                </View>
            </View>
            {!isCorrectNumber && <Text style={styles.alertText}>Masukkan salah</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 20,
        paddingHorizontal: 25,
        backgroundColor: 'lightgrey'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 16,
        marginBottom: 10,
    },
    textInput: {
        height: 40,
        flex: 4,
        backgroundColor: 'white'
    },
    inputIcon: {
        marginLeft: 15,
        fontSize: 40,
    },
    alertText: {
        backgroundColor: 'black',
        padding: 5,
        color: 'white'
    },
    icon: {
        flex: 1,
    }
})


export default TransactionNumberInput