import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { convertIntToIdrString } from '../../../utils/currency';

const BillView = ({ header, price }) => {

    return (
        <TouchableOpacity style={styles.container}>
            <Text style={{...styles.text}}>{header}</Text>
            <Text style={{...styles.text, ...styles.price}}>{convertIntToIdrString(price)}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 10,
    },
    text: {
        textAlign: 'center',
        marginVertical: 5,
    },
    price: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: 'bold'
    },
})

export default BillView