import React, { useState, useEffect } from 'react'
import { View, FlatList, Text, StyleSheet, Dimensions, Button } from 'react-native';
import GridMenuCard from '../../menu/GridMenuCard';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ListMenuCard from '../../menu/ListMenuCard';
import { transformGridItem } from '../../../utils/grid';
import { shouldShowOtherModalState } from '../../../state/screen';
import { useRecoilState } from 'recoil';


const TransactionList = ({ title, transactions, navigation }) => {

    const [isGridMode, setIsGridMode] = useState(false)
    const [numColumns, setNumColumns] = useState(1)
    const [shouldShowOtherModal, setShouldShowOtherModal] = useRecoilState(shouldShowOtherModalState)
    const handleViewOptionPress = (isGridMode) => {
        setIsGridMode(isGridMode)
    }

    const renderItem = ({ item }) => {
        if (item.isPlaceholder) {
            return <View style={{ flex: 1, margin: 5, padding: 10 }}></View>
        }

        const onPressHandler = () => {
            if (item.screenName === "Other"){
                setShouldShowOtherModal(true)
            } else {
                navigation.navigate(item.screenName)
            }
        }


        if (isGridMode) {
            return (
                <GridMenuCard
                    onPress={onPressHandler}
                    text={item.title}
                />
            ) 
        } 

        return (
            <ListMenuCard 
                onPress={onPressHandler}
                text={item.title}
            />
        )
    }

    useEffect(() => {
        setNumColumns(isGridMode ? 3 : 1)
    }, [isGridMode])

    return (
        <View style={styles.container}>
            {/* Header  */}
            {/* Content */}
            <View style={styles.transactionSectionContainer}>
                <FlatList
                    ListHeaderComponent={() => (
                        <View style={styles.header}>
                        <Text style={styles.headerText}>{title}</Text>
                        <View style={styles.viewOption}>
                            <TouchableOpacity 
                                accessibilityLabel="Grid View"
                                onPress={() => handleViewOptionPress(true)}
                            >
                                <Icon style={styles.viewOptionButton} name="th" color={isGridMode ? "black" : "lightgrey"} />
                            </TouchableOpacity>
                            <TouchableOpacity
                                accessibilityLabel="List View" 
                                onPress={() => handleViewOptionPress(false)}
                            >
                                <Icon style={styles.viewOptionButton} name="th-list" color={!isGridMode ? "black" : "lightgrey"} />
                            </TouchableOpacity>
                        </View>
                    </View>        
                    )}
                    data={transformGridItem(transactions, numColumns)}
                    contentContainerStyle={styles.transactionSection}
                    key={"flatlist-"+numColumns}
                    keyExtractor={(item, index) => "transaction-" + index.toString()}
                    numColumns={numColumns}
                    renderItem={renderItem}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: Dimensions.get('screen').width,
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    headerText: {
        fontSize: 17
    },
    viewOption: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewOptionButton: {
        marginLeft: 10,
        fontSize: 32,
    },
    transactionSectionContainer: {
        flexGrow: 1,
    },
    transactionSection: {
        flex: 1,
        paddingBottom: 10,
        alignItems: "stretch",
    }
})

export default TransactionList;