import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

const TransactionDetailInfo = ({ first, second }) => {
    return (
        <View>
            <View style={styles.card}>
                <Text style={styles.sectionHeader}>Informasi Pelanggan</Text>
                <View style={styles.section}>
                    <View style={[styles.field, styles.fieldName]}>
                        {first.fields.map((field) => (<Text style={styles.fieldText}>{field}</Text>))}
                    </View>
                    <View style={[styles.field, styles.fieldValue]}>
                        {first.values.map((value) => (<Text style={styles.fieldText}>{value}</Text>))}
                    </View>
                </View>
            </View>
            <View style={styles.card}>
                <Text style={styles.sectionHeader}>Detail Pembayaran</Text>
                <View style={styles.section}>
                    <View style={[styles.field, styles.fieldName]}>
                        {second.fields.map((field) => (<Text style={styles.fieldText}>{field}</Text>))}
                    </View>
                    <View style={[styles.field, styles.fieldValue]}>
                        {second.values.map((value) => (<Text style={styles.fieldText}>{value}</Text>))}
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 30,
    },
    content: {
        marginVertical: 36,
    },
    section: {
        flexDirection: 'row',
    },
    sectionHeader: {
        marginBottom: 16,
    },
    card: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 30,
        marginHorizontal: 25,
        marginVertical: 20,
    },
    field: {
        marginHorizontal: 10,
    },
    fieldName: {
        flex: 1,
        alignItems: 'flex-end'
    },
    fieldValue: {
        flex: 1,
        alignItems: 'flex-start'
    },
    fieldText: {
        marginBottom: 10,
    },
})

export default TransactionDetailInfo