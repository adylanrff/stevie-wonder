import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

const PaymentGuideCard = ({ steps }) => {
    return (
        <View style={styles.container}>
            {steps.map((step, idx) => (
                <View style={styles.step}>
                    <Text style={[styles.common, styles.number]}>{idx}</Text>
                    <Text style={[styles.common, styles.text]}>{step}</Text>
                </View>
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 10,
        paddingHorizontal: 30,
        backgroundColor: 'white',
    },
    step: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 15,
    },
    common: {
        fontSize: 15,
    },
    number: {
        flex: 1,
        alignSelf: 'center',
        color: 'black'
    }, 
    text: {
        flex: 6,
    }, 
})

export default PaymentGuideCard;