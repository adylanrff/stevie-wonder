import React from 'react'
import Modal from 'react-native-modal'
import { Text, View, StyleSheet, Image, Button } from 'react-native';
import placeholderImg from '../../assets/image/placeholder.jpg'

import { useRecoilState } from 'recoil';
import { shouldShowOtherModalState } from '../../state/screen';

const OtherTransactionInfoModal = ({ navigation }) =>  { 
    const [shouldShowOtherModal, setShouldShowOtherModal] = useRecoilState(shouldShowOtherModalState)
    const onCloseModal = () => {
        navigation.navigate('Other')
    }

    return (
        <Modal 
            isVisible={shouldShowOtherModal}
            onBackdropPress={() => setShouldShowOtherModal(false)}
            onModalHide={onCloseModal}
        >
            <View style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.headerText}>Pembelian Lainnya</Text>
                    <Image style={styles.image} source={placeholderImg}/>
                    <Text style={styles.info}>
                    Silahkan hubungi kasir dan memberikan nomor ponsel yang terdaftar di akun E-wallet untuk dimasukkan ke dalam EDC (Electronic Data Capture) E-wallet. Kemudian jika sudah muncul layar konfirmasi, pilih ‘Konfirmasi’
                    </Text>
                </View>
                <Button
                    onPress={() => setShouldShowOtherModal(false)}     
                    title="Tutup" 
                    color="black"
                />
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 0.5,
        marginHorizontal: 24,
        backgroundColor: 'white',
    },
    content: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headerText: {
        fontSize: 18,
        flex: 1,
        alignSelf: 'flex-start',
        fontWeight: 'bold'
    },
    image: {
        flex: 4,
    },
    info: {
        marginTop: 10,
        flex: 3,
    }
})

export default OtherTransactionInfoModal;