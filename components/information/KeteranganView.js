import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

const KeteranganView = ({ header, content }) => {
    
    return (
        <View style={styles.container}>
            <Text style={styles.header}>{header}</Text>
            <Text style={styles.content}>{content}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'flex-start',
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 10,
    },
    header: {
        fontWeight: 'bold',
        marginBottom: 10,
        fontSize: 15,
        textAlign: 'left',
    },
    content: {
        fontSize: 15,
        marginTop: 10,
        textAlign: 'left',
    }

})

export default KeteranganView