import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

const CardInfoForm = (props) => {

    const { setCreditCardNumber } = props
    const { setExpiryDate } = props
    const { setCVV } = props

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Informasi Kartu</Text>
            <Text style={styles.formTitle}>Nomor Kartu</Text>
            <View style={styles.inputSection}>
                <TextInput 
                    style={styles.textInput} 
                    keyboardType="number-pad"
                    onChangeText={setCreditCardNumber} 
                />
            </View>
            <Text style={styles.formTitle}>Masa Berlaku</Text>
            <View style={styles.inputSection}> 
                <TextInput 
                    style={styles.textInput} 
                    keyboardType="default"
                    onChangeText={setExpiryDate} 
                />
            </View>
            <Text style={styles.formTitle}>CVV</Text>
            <View style={styles.inputSection}>
                <TextInput 
                    style={styles.textInput} 
                    keyboardType="decimal-pad"
                    onChangeText={setCVV}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 24,
        backgroundColor: 'lightgrey'
    },
    header: {
        fontSize: 18,
        marginBottom: 16,
        fontWeight: 'bold'
    },
    formTitle: {
        fontSize: 15,
    },
    textInput: {
        height: 40,
        fontSize: 12,
        flex: 4,
        backgroundColor: 'white'
    },
    inputSection: {
        marginVertical: 5,
    }
})

export default CardInfoForm;