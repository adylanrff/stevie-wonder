import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { convertIntToIdrString } from '../../utils/currency'

const BalanceView = ({ text, balance }) => {
    return (
        <View styles={styles.container}>
            <Text style={styles.headerText}>{text}</Text>
            <Text style={styles.balanceText}>{convertIntToIdrString(balance)}</Text>
        </View>
)}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        marginBottom: 5,
        textAlign: 'center',
    },
    balanceText: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
    }
})

export default BalanceView;
