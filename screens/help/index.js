import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

const HelpScreen = ({ route }) => {
    const { helps } = route.params

    const defaultHelp = `
1. Untuk melihat total saldo, total saldo terletak di paling atas

2. Pada bagian tengah Anda dapat melihat total yang sudah Anda keluarkan bulan ini, serta berbagai menu untuk menambah saldo, memindahkan saldo ke akun lain, dan melihat riwayat transaksi
        
3. Pada bagian bawah, Anda dapat memilih satu dari empat menu dan dapat mengubah bentuknya dari grid menjadi list
    `
    return (
        <ScrollView>
            {helps.map((help) => (
                <View style={styles.container}>
                <Text style={styles.headerText}>{help.header || "Bantuan"}</Text>
                <Text style={styles.content}>{help.content || defaultHelp}</Text>
                </View>
            ))
            }
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 32,
        backgroundColor: 'white',
        marginVertical: 10,
    },
    headerText: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    content: {
        marginVertical: 20,
    }
})

export default HelpScreen;