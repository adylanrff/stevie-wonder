import React, {useLayoutEffect} from 'react';
import { View, Text, StyleSheet, Modal } from 'react-native';
import {  useRecoilValue, useRecoilState } from 'recoil';

import HeaderTitle from '../../components/header/HeaderTitle'
import HeaderButton from '../../components/header/HeaderButton';
import BalanceView from '../../components/balance';
import OtherTransactionInfoModal from '../../components/information/OtherTransactionInfoModal';

import MenuModal from '../../components/menu/MenuModal';
import { userBalanceState, userExpenseState } from '../../state/auth';
import TransactionList from '../../components/transaction/TransactionList'
import { latestTransactionState, transactionState } from '../../state/transaction';
import { ScrollView } from 'react-native-gesture-handler';
import { shouldShowOtherModalState } from '../../state/screen';

const HomeScreen = ({ navigation }) => {
  const balance = useRecoilValue(userBalanceState)
  const expense = useRecoilValue(userExpenseState)
  
  const [allTransactions, _] = useRecoilState(transactionState)
  const latestTransaction = useRecoilValue(latestTransactionState)

  const headerOptions = {
    headerTitle: () => <HeaderTitle title="E-Wallet" />,
    headerLeft: () => <HeaderButton accessibilityLabel="Pengaturan" iconName="cog" onPress={handleHeaderSettingButton}/>,
    headerRight: () => <HeaderButton accessibilityLabel="Bantuan" iconName="question-circle" onPress={handleHeaderHelpButton}/>,
  }
  
  useLayoutEffect(() => {
    navigation.setOptions(headerOptions)
  })
  
  const handleHeaderSettingButton = () => {
    navigation.navigate('Settings')
  }

  const handleHeaderHelpButton = () => {
    navigation.navigate('Help', {
      helps: [{}]
    })
  }

  return (
    <>
    <ScrollView contentContainerStyle={styles.container}>
      <BalanceView text={"Total Saldo"} balance={balance}/>
      <MenuModal navigation={navigation} expenses={expense} />
      <TransactionList 
        title={"Transaksi Terakhir"} 
        transactions={latestTransaction}
        navigation={navigation}
      />
      <TransactionList 
        title={"Semua Transaksi"}
        transactions={allTransactions}
        navigation={navigation}
      />
    </ScrollView>
    <OtherTransactionInfoModal navigation={navigation}/>
    </>
  );
}

const styles = StyleSheet.create({
  container: { 
    alignItems: 'center', 
    paddingVertical: 30,
    paddingHorizontal: 30,
  },
})

export default HomeScreen;