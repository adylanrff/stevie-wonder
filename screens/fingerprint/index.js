import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useRecoilState } from 'recoil';
import { isAuthenticatedState, userState } from '../../state/auth';
import FingerprintForm from '../../components/login/fingerprint/FingerprintForm';

const FingerprintScreen = ({ navigation, route }) => {
  const [user, setUserState] = useRecoilState(userState)

  const { params } = route

  const handlePinLogin = () => {
    navigation.navigate('Login')
  }

  const handleLogin = () => {
    setUserState({
      ...user,
      isAuthenticated: true,
    })
  }

  const onFingerprintSubmit = () => {
    if (params && params.onSubmit) {
      params.onSubmit()
    }
    handleLogin()
  }

  return (
    <View style={styles.container}>
      <FingerprintForm onSubmit={onFingerprintSubmit} onBack={handlePinLogin}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default FingerprintScreen;