import React, { useLayoutEffect } from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useRecoilValue } from 'recoil';
import { userBalanceState } from '../../../state/auth';
import { ScrollView } from 'react-native-gesture-handler';
import TokenListrikScreen from './TokenListrikScreen';
import TagihanScreen from './TagihanScreen';
import BalanceView from '../../../components/balance';
import HeaderTitle from '../../../components/header/HeaderTitle';
import HeaderButton from '../../../components/header/HeaderButton';
import { getHelpString } from '../../../utils/helpString';


const TabNavigator  = createMaterialTopTabNavigator()

const ListrikScreen = ({ navigation, route }) => {

    const balance = useRecoilValue(userBalanceState)

    const headerOptions = {
        headerTitle: () => <HeaderTitle title="Pembayaran Listrik PLN" />,
        headerRight: () => <HeaderButton accessibilityLabel="Bantuan" iconName="question-circle" onPress={handleHeaderHelpButton}/>,
    }

    useLayoutEffect(() => {
        navigation.setOptions(headerOptions)
    })

    const helpString = getHelpString(route)

    const handleHeaderHelpButton = () => {
        navigation.navigate('Help', {
            helps: helpString
        })
    }

    return (
        <ScrollView>
            <View style={styles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <TabNavigator.Navigator backBehavior="none" >
                <TabNavigator.Screen name="TokenListrik" component={TokenListrikScreen} options={{title: "Token Listrik"}}/>
                <TabNavigator.Screen name="Tagihan" component={TagihanScreen} options={{title: "Tagihan"}}/>
            </TabNavigator.Navigator>
        </ScrollView>            
    )
}

const styles = StyleSheet.create({
    header: {
        padding: 10,
        backgroundColor: 'white'
    }
})


export default ListrikScreen;