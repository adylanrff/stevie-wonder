import React, { useState } from 'react'
import { screenStyles } from '../../styles'
import { View, Text } from 'react-native';
import TransactionNumberInput from '../../../../components/transaction/TransactionNumberInput';
import TransactionNumberHistory from '../../../../components/transaction/TransactionNumberHistory';
import TransactionOptionList from '../../../../components/transaction/TransactionOptionList';
import { useRecoilState, useRecoilValue } from 'recoil';
import { pulsaOptionsState } from '../../../../state/itemOption';
import { numberHistoryState } from '../../../../state/transaction';
import GridItemOptionMenu from '../../../../components/menu/GridItemOptionMenu';
import { validatePhoneNumber } from '../../../../utils/validation';
import { convertIntToIdrString } from '../../../../utils/currency';
import { userNameState } from '../../../../state/auth';


const TokenListrikScreen = ({ navigation }) => {
    const [isCorrectNumber, setIsCorrectNumber] = useState(false)
    const [pulsaOptions, setPulsaOptions] = useRecoilState(pulsaOptionsState)
    const [numberHistory, setNumberHistory] = useRecoilState(numberHistoryState)
    const [phoneNumber, setPhoneNumber] = useState('')
    const userName = useRecoilValue(userNameState)

    const handleOnIconPress = () => (
        setPhoneNumber("087838888671")
    )

    const renderListrikOptionItem = ({ item }) => {
        const params = {
            transactionType: 'Listrik',
            first: {
                fields: ["ID Pelanggan", "Nama Pelanggan"],
                values: [phoneNumber, userName]
            },
            second: {
                fields: ["Voucher Listrik", "Harga", "Biaya", "Total"],
                values: [item.title, ...[item.price, item.price/100, parseInt(item.price)+parseInt(item.price/100)].map((e) => convertIntToIdrString(e))]
            },
            success: {
                headerText: "Pengisian token listrik berhasil",
                subtitleText: `Untuk nomor ponsel ${phoneNumber}`,
                price: `${item.price}`
            }
        }
        return <GridItemOptionMenu item={item} onPress={() => navigation.navigate('Transaction', params)} />
    }
    
    return (
        <View style={screenStyles.container}>
            <TransactionNumberInput
                headerText="Nomor Meter / ID Pelanggan" 
                iconName="square"
                iconOnPress={handleOnIconPress}
                number={phoneNumber}
                setNumber={setPhoneNumber}
                onCorrectInput={() => setIsCorrectNumber(true)}
                onFalseInput={() => setIsCorrectNumber(false)}
                validation={validatePhoneNumber}
            />
            {!isCorrectNumber ? 
                <TransactionNumberHistory 
                    numberHistory={numberHistory}
                    headerText="Nomor Sebelumnya" 
                    onNumberPress={setPhoneNumber}
                /> : 
                <TransactionOptionList
                    headerTitle="Pilih Voucher Listrik"
                    headerSubtitle="Biaya transaksi Rp 100.000" 
                    optionData={pulsaOptions}
                    renderItem={renderListrikOptionItem}
                    numColumns={2}
                />
            }
        </View>
    )
}

export default TokenListrikScreen;