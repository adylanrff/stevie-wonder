import React, { useState } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import TransactionNumberInput from '../../../../components/transaction/TransactionNumberInput';
import { screenStyles } from '../../styles';
import { validateNominal } from '../../../../utils/validation';
import CardInfoForm from '../../../../components/information/CardInfoForm';
import { convertIntToIdrString } from '../../../../utils/currency';
import { ScrollView } from 'react-native-gesture-handler';

const TopUpCreditScreen = ({ navigation }) => {
    const [nominal, setNominal] = useState('')
    const [creditCardNumber, setCreditCardNumber] = useState('')
    const [expiryDate, setExpiryDate] = useState('')
    const [cvv, setCVV] = useState('')

    const handlePaymentPress = () => {
        const params = {
            transactionType: 'TopUpCredit',
            first: {
                fields: ["No. Kartu Kredit", "Masa Berlaku", "CVV"],
                values: [creditCardNumber, expiryDate, cvv]
            },
            second: {
                fields: ["Nominal", "Biaya", "Total"],
                values: [nominal, nominal/100, parseInt(nominal)+parseInt(nominal/100)].map((e) => convertIntToIdrString(e))
            },
            success: {
                headerText: "Pengisian saldo berhasil",
                subtitleText: `Sebesar ${nominal}`,
                price: `${nominal}`
            }
        }

        navigation.navigate('Transaction', params)
    }
    
    return (
        <ScrollView contentContainerStyle={screenStyles.topUpContainer}>
            <View style={styles.textSection}>
                <Text style={styles.headerText}>Top Up dengan kartu kredit</Text>
                <Text style={styles.subtitleText}>Biaya transaksi Rp 1.000.000</Text>
            </View>
            <View style={screenStyles.topUpSection}>
                <TransactionNumberInput 
                    headerText="Nominal Top-Up"
                    validation={validateNominal}
                    number={nominal}
                    setNumber={setNominal}
                />
            </View>
            <View style={screenStyles.topUpSection}>
                <CardInfoForm
                    setCreditCardNumber={setCreditCardNumber}
                    setExpiryDate={setExpiryDate}
                    setCVV={setCVV}
                />
            </View>
            <View style={screenStyles.topUpSection}>
                <Button onPress={handlePaymentPress} title="Lanjutkan Pembayaran" color="black"/>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    textSection: {
        alignItems: 'flex-start',
    },
    headerText: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    subtitleText: {
        fontSize: 12,
        marginVertical: 8,
    },
})

export default TopUpCreditScreen