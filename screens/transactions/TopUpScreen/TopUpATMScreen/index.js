import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useRecoilState, useRecoilValue } from 'recoil';
import { bankState, latestBankState} from '../../../../state/bank'
import { FlatList, TextInput } from 'react-native-gesture-handler';
import GridMenuCard from '../../../../components/menu/GridMenuCard';
import { transformGridItem } from '../../../../utils/grid';
import { render } from 'react-dom';
import ListMenuCard from '../../../../components/menu/ListMenuCard';
import { screenStyles } from '../../styles';

const TopUpATMScreen = () => {
    const [banks, setBankState] = useRecoilState(bankState)
    const latestBanks = useRecoilValue(latestBankState)
    const latestBankNumColumns = 3
    const allBanksNumColumns = 1

    const renderLatestBanks = ({item}) => {
        if (item.isPlaceholder) {
            return <View style={{ flex: 1, margin: 5, padding: 10 }}></View>
        }

        return (
            <GridMenuCard
                text={item.name} 
            />
        )
    }

    const renderAllBanks = ({item}) => {
        return (
            <ListMenuCard
                text={item.name} 
            />
        )
    }

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.headerText}>Bank Sebelumnya</Text>
                <FlatList
                    data={transformGridItem(latestBanks, latestBankNumColumns)}
                    numColumns={latestBankNumColumns}
                    key={latestBankNumColumns}
                    keyExtractor={(item, idx) => "latestbank-"+idx.toString()}
                    renderItem={renderLatestBanks}
                />
            </View>
            <View>
                <TextInput
                    style={styles.bankInput}
                    placeholder="Pilih Bank"
                    accessibilityLabel="Bank Input"
                />
                <FlatList
                    data={banks}
                    numColumns={allBanksNumColumns}
                    key={allBanksNumColumns}
                    keyExtractor={(item, idx) => "latestbank-"+idx.toString()}
                    renderItem={renderAllBanks}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    headerText: {
        fontSize: 18,
        fontWeight: 'bold',
        marginVertical: 10,
    },
    container: {
        margin: 20,
    },
    bankInput: {
        height: 40,
        backgroundColor: 'white',
        marginVertical: 25,
    }
})

export default TopUpATMScreen