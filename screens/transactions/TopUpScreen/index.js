import React, { useLayoutEffect } from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native';

import TopUpCreditScreen from './TopUpCreditScreen'
import TopUpDebitScreen from './TopUpDebitScreen'
import TopUpMBankingScreen from './TopUpMBankingScreen'
import TopUpATMScreen from './TopUpATMScreen'

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import BalanceView from '../../../components/balance';
import { useRecoilValue } from 'recoil';
import { userBalanceState } from '../../../state/auth';
import { ScrollView } from 'react-native-gesture-handler';
import HeaderButton from '../../../components/header/HeaderButton';
import HeaderTitle from '../../../components/header/HeaderTitle';
import { getHelpString } from '../../../utils/helpString';


const TabNavigator  = createMaterialTopTabNavigator()

const TopUpScreen = ({ navigation, route }) => {

    const balance = useRecoilValue(userBalanceState)
    const tabBarOptions = {
        upperCaseLabel: false,
        labelStyle: {
            fontSize: 10,
            margin: 0,
            padding: 0,
          },
    }

    const headerOptions = {
        headerTitle: () => <HeaderTitle title="Pembayaran Listrik PLN" />,
        headerRight: () => <HeaderButton accessibilityLabel="Bantuan" iconName="question-circle" onPress={handleHeaderHelpButton}/>,
    }

    useLayoutEffect(() => {
        navigation.setOptions(headerOptions)
    })

    const helpString = getHelpString(route)

    const handleHeaderHelpButton = () => {
        navigation.navigate('Help', {
            helps: helpString
        })
    }


    return (
        <ScrollView>
            <View style={styles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <TabNavigator.Navigator backBehavior="none" tabBarOptions={tabBarOptions}>
                <TabNavigator.Screen name="TopUpCredit" component={TopUpCreditScreen} options={{title: "Kredit"}}/>
                <TabNavigator.Screen name="TopUpDebit" component={TopUpDebitScreen} options={{title: "Debit"}}/>
                <TabNavigator.Screen name="TopUpMBanking" component={TopUpMBankingScreen} options={{title: "M-Banking"}}/>
                <TabNavigator.Screen name="TopUpATM" component={TopUpATMScreen } options={{title: "ATM"}}/>
            </TabNavigator.Navigator>
        </ScrollView>            
    )
}

const styles = StyleSheet.create({
    header: {
        padding: 10,
        backgroundColor: 'white'
    }
})


export default TopUpScreen;