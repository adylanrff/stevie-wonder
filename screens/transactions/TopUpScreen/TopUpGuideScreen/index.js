import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { screenStyles } from '../../styles'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import BalanceView from '../../../../components/balance'
import { useRecoilValue } from 'recoil'
import { userBalanceState } from '../../../../state/auth'
import PaymentGuideCard from '../../../../components/information/PaymentGuideCard'

const TopUpGuideScreen = ({ navigation, route }) => {
    const balance = useRecoilValue(userBalanceState)
    const { title, subtitle, steps } = route.params

    return (
        <ScrollView>
            <View style={screenStyles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <View style={styles.container}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.subtitle}>{subtitle}</Text> 
                <View style={styles.paymentGuide}>
                    <PaymentGuideCard steps={steps} />
                </View>
                <View>
                    <TouchableOpacity style={styles.button} onPress={() => { navigation.goBack()}}>
                        <Text style={styles.buttonText}>Kembali</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 30,
    },
    title: {
        fontSize: 21,
        marginBottom: 10,
    },
    subtitle: {
        fontSize: 15,
    },
    paymentGuide: {
        marginVertical: 20,
    },
    button: {
        backgroundColor: 'black',
        padding: 10,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
})

export default TopUpGuideScreen