import React from 'react'
import { View, Text, Button, StyleSheet } from 'react-native'
import { screenStyles } from '../../styles'
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler'
import { useRecoilState, useRecoilValue } from 'recoil'
import { PDAMDistrictState, latestPDAMDistrictState } from '../../../../state/district'
import ListMenuCard from '../../../../components/menu/ListMenuCard'

const PDAMDistrict = ({ onPressDistrict }) => {
    const [pdamDistrict, setPDAMDistrict] = useRecoilState(PDAMDistrictState)
    const latestPDAMDistrict = useRecoilValue(latestPDAMDistrictState)

    const renderItem = ({ item }) => {
        return (
            <ListMenuCard 
                text={item.city} 
                subtitle={item.province} 
                onPress={onPressDistrict}
            />
        )
    }

    return (
        <View style={styles.container}>
            <View style={screenStyles.section}>
                <Text style={styles.headerText}>Kota/Kabupaten Sebelumnya</Text>
                <TouchableOpacity style={styles.button}onPress={onPressDistrict}>
                    <Text>{latestPDAMDistrict.city}</Text>
                </TouchableOpacity>
            </View>
            <View style={screenStyles.section}>
                <FlatList
                    data={pdamDistrict}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
    },
    button: {
        alignItems: 'center',
        marginTop: 20,
        padding: 10,
        backgroundColor: 'white',
    },
    headerText: {
        fontSize: 18,
    },
})

export default PDAMDistrict;