import React, { useState, useEffect, useLayoutEffect } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native';
import { screenStyles } from '../../styles';
import TransactionNumberInput from '../../../../components/transaction/TransactionNumberInput';
import { PDAMBillState } from '../../../../state/itemOption'
import KeteranganView from '../../../../components/information/KeteranganView';
import BillView from '../../../../components/transaction/BillView';
import { useRecoilState, useRecoilValue } from 'recoil';
import { userBalanceState, userNameState } from '../../../../state/auth';
import { convertIntToIdrString } from '../../../../utils/currency';
import { validatePhoneNumber } from '../../../../utils/validation';
import HeaderTitle from '../../../../components/header/HeaderTitle';
import HeaderButton from '../../../../components/header/HeaderButton';


const PDAMPaymentScreen = ({ navigation }) => {
    const [isCorrectNumber, setIsCorrectNumber] = useState(false)
    const balance = useRecoilValue(userBalanceState)
    const userName = useRecoilValue(userNameState)
    const [PDAMBill, setPDAMBill] = useRecoilState(PDAMBillState)
    const [billNumber, setBillNumber] = useState('')
    const [billPrice, setBillPrice] = useState(null)

    const keteranganContent = `1. Pembayaran maksimal setiap tanggal 20 setiap bulannya

2. Jumlah tagihan sudah termasuk dengan denda
    
3. Pembayaran dikenakan biaya tambahan sebesar Rp. 1.000.000
`
    const handleOnIconPress = () => {
        setBillNumber("087838888671")
    }

    const handlePaymentPress = () => {
        const params = {
            transactionType: 'PDAM',
            first: {
                fields: ["No. Pelanggan", "Nama Pelanggan"],
                values: [billNumber, userName]
            },
            second: {
                fields: ["Jumlah Tagihan", "Biaya", "Total"],
                values: [billPrice, billPrice/100, parseInt(billPrice)+parseInt(billPrice/100)].map((e) => convertIntToIdrString(e))
            },
            success: {
                headerText: "Pembayaran tagihan air berhasil",
                subtitleText: `untuk nomor pelanggan ${billNumber}`,
                price: `${billPrice}`
            }
        }

        navigation.navigate('Transaction',params)
    }

    useEffect(() => {
        if (billNumber in PDAMBill) {
            setBillPrice(PDAMBill[billNumber])
        } else {
            setBillPrice(null)
        }
    }, [billNumber, PDAMBill])

    
    return (
        <View style={screenStyles.container}>
            <TransactionNumberInput
                headerText="Nomor Tagihan" 
                iconName="square"
                iconOnPress={handleOnIconPress}
                number={billNumber}
                setNumber={setBillNumber}
                onCorrectInput={() => setIsCorrectNumber(true)}
                onFalseInput={() => setIsCorrectNumber(false)}
                validation={validatePhoneNumber}
            />
            <View style={screenStyles.section}>
                {billPrice && (<BillView header="TOTAL TAGIHAN" price={billPrice} />)}
                <KeteranganView header="Keterangan:" content={keteranganContent}/>
                <View style={screenStyles.bottomButton}>
                    <Button 
                        title="Lanjut ke pembayaran" 
                        disabled={!billPrice}
                        onPress={handlePaymentPress}
                        color="black"
                    />
                </View>
            </View>
        </View>    
  )
}

export default PDAMPaymentScreen;