import React, { useState, useEffect, useLayoutEffect } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import BalanceView from '../../../components/balance';
import { screenStyles } from '../styles';
import { PDAMBillState } from '../../../state/itemOption'
import { useRecoilState, useRecoilValue } from 'recoil';
import { userBalanceState, userNameState } from '../../../state/auth';
import HeaderTitle from '../../../components/header/HeaderTitle';
import HeaderButton from '../../../components/header/HeaderButton';
import PDAMPaymentScreen from './PDAMPaymentScreen';
import PDAMDistrict from './PDAMDistrict';
import { getHelpString } from '../../../utils/helpString';


const PDAMScreen = ({ navigation, route }) => {
    const balance = useRecoilValue(userBalanceState)
    const [PDAMBill, setPDAMBill] = useRecoilState(PDAMBillState)
    const [billNumber, setBillNumber] = useState('')
    const [billPrice, setBillPrice] = useState(null)
    const [isDistrictClicked, setIsDistrictClicked] = useState(false)

    const onClickDistrict = () => {
        setIsDistrictClicked(true)
    }

    const headerOptions = {
        headerTitle: () => <HeaderTitle title="Pembayaran Air PDAM" />,
        headerRight: () => <HeaderButton accessibilityLabel="Bantuan" iconName="question-circle" onPress={handleHeaderHelpButton}/>,
    }

    useLayoutEffect(() => {
        navigation.setOptions(headerOptions)
    })

    const helpString = getHelpString(route)

    const handleHeaderHelpButton = () => {
        navigation.navigate('Help', {
            helps: helpString
        })
    }

    useEffect(() => {
        if (billNumber in PDAMBill) {
            setBillPrice(PDAMBill[billNumber])
        } else {
            setBillPrice(null)
        }
    }, [billNumber, PDAMBill])

    return (
        <ScrollView>
            <View style={screenStyles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <View>
                {isDistrictClicked ?  (<PDAMPaymentScreen navigation={navigation}/>) :  <PDAMDistrict onPressDistrict={onClickDistrict}/>} 
            </View>
        </ScrollView>            
    )
}

export default PDAMScreen;