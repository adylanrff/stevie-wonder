import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { screenStyles } from '../../styles'
import { FlatList, TextInput } from 'react-native-gesture-handler'
import ListHistoryCard from '../../../../components/menu/ListHistoryCard'
import { useRecoilState, useRecoilValue } from 'recoil'
import { allHistoryState, paymentHistoryState } from '../../../../state/history'


const PaymentHistoryScreen = (pros) => {
    const history = useRecoilValue(paymentHistoryState)

    const renderItem = ({item}) => {
        return (
            <ListHistoryCard
                title={item.title}
                subtitle={item.subtitle}
                date={item.date} 
            />
        )
    }

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.dateFilter}
                placeholder="Pilih Tanggal"
            />
            <View style={screenStyles.section}>
                <FlatList
                    data={history}
                    numColumns={1}
                    renderItem={renderItem} 
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 24,
        marginHorizontal: 20,
    },
    dateFilter: {
        height: 40,
        backgroundColor: 'white'
    },
})

export default PaymentHistoryScreen