import React from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native';

import AllHistoryScreen from './AllHistoryScreen'

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import BalanceView from '../../../components/balance';
import { useRecoilValue } from 'recoil';
import { userBalanceState } from '../../../state/auth';
import { ScrollView } from 'react-native-gesture-handler';
import TransferHistoryScreen from './TransferHistoryScreen';
import PaymentHistoryScreen from './PaymentHistoryScreen';
import TopUpHistoryScreen from './TopUpHistoryScreen';


const TabNavigator  = createMaterialTopTabNavigator()

const TopUpScreen = ({ navigation }) => {

    const balance = useRecoilValue(userBalanceState)
    const tabBarOptions = {
        upperCaseLabel: false,
        labelStyle: {
            fontSize: 10,
            margin: 0,
            padding: 0,
          },
    }

    return (
        <ScrollView>
            <View style={styles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <TabNavigator.Navigator backBehavior="none" tabBarOptions={tabBarOptions}>
                <TabNavigator.Screen name="AllHistory" component={AllHistoryScreen} options={{title: "Semua"}}/>
                <TabNavigator.Screen name="TransferHistory" component={TransferHistoryScreen} options={{title: "Transfer"}}/>
                <TabNavigator.Screen name="PaymentHistory" component={PaymentHistoryScreen} options={{title: "Pembayaran"}}/>
                <TabNavigator.Screen name="TopUpHistory" component={TopUpHistoryScreen} options={{title: "Top Up"}}/>
            </TabNavigator.Navigator>
        </ScrollView>            
    )
}

const styles = StyleSheet.create({
    header: {
        padding: 10,
        backgroundColor: 'white'
    }
})


export default TopUpScreen;