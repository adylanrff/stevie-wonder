import React, { useState } from 'react'
import { screenStyles } from '../../styles'
import { View, Text } from 'react-native';
import TransactionNumberInput from '../../../../components/transaction/TransactionNumberInput';
import TransactionNumberHistory from '../../../../components/transaction/TransactionNumberHistory';
import TransactionOptionList from '../../../../components/transaction/TransactionOptionList';
import { useRecoilState } from 'recoil';
import { packetOptionState } from '../../../../state/itemOption';
import { numberHistoryState } from '../../../../state/transaction';
import ListItemOptionMenu from '../../../../components/menu/ListItemOptionMenu';
import { validatePhoneNumber } from '../../../../utils/validation';
import { convertIntToIdrString } from '../../../../utils/currency';


const PacketDataScreen = ({ navigation }) => {
    const [isCorrectNumber, setIsCorrectNumber] = useState(false)
    const [packetOptions, setPacketOptions] = useRecoilState(packetOptionState)
    const [numberHistory, setNumberHistory] = useRecoilState(numberHistoryState)
    const [phoneNumber, setPhoneNumber] = useState('')

    const renderPulsaOptionItem = ({ item }) => {
        const params = {
            transactionType: 'Paket data',
            first: {
                fields: ["Nomor Ponsel", "Provider"],
                values: [phoneNumber, "Telkomsel"]
            },
            second: {
                fields: ["Paket Data", "Harga", "Biaya", "Total"],
                values: [item.title, ...[item.price, item.price/100, parseInt(item.price)+parseInt(item.price/100)].map((e) => convertIntToIdrString(e))]
            },
            success: {
                headerText: "Pengisian pulsa berhasil",
                subtitleText: `Untuk nomor ponsel ${phoneNumber}`,
                price: `${item.price}`
            }
        }

        return <ListItemOptionMenu item={item} onPress={() => navigation.navigate('Transaction', params) } />
    }
    
    return (
        <View style={screenStyles.container}>
            <TransactionNumberInput
                headerText="Nomor Ponsel" 
                iconName="square"
                number={phoneNumber}
                setNumber={setPhoneNumber}
                onCorrectInput={() => setIsCorrectNumber(true)}
                onFalseInput={() => setIsCorrectNumber(false)}
                validation={validatePhoneNumber}
            />
            {!isCorrectNumber ? 
                <TransactionNumberHistory 
                    numberHistory={numberHistory}
                    headerText="Nomor Sebelumnya" 
                    onNumberPress={setPhoneNumber}
                /> : 
                <TransactionOptionList
                    headerTitle="Pilih Paket Data"
                    headerSubtitle="Biaya transaksi Rp 100.000" 
                    optionData={packetOptions}
                    renderItem={renderPulsaOptionItem}
                    numColumns={1}
                />
            }
        </View>
    )
}

export default PacketDataScreen;