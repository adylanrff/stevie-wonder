import React, { useLayoutEffect } from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { screenStyles } from '../styles';
import PulsaBalanceScreen from './PulsaBalanceScreen'
import PacketDataScreen from './PacketDataScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import BalanceView from '../../../components/balance';
import { useRecoilValue } from 'recoil';
import { userBalanceState } from '../../../state/auth';
import { withNavigation } from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';
import HeaderButton from '../../../components/header/HeaderButton';
import HeaderTitle from '../../../components/header/HeaderTitle';
import { getHelpString } from '../../../utils/helpString';


const TabNavigator  = createMaterialTopTabNavigator()

const PulsaScreen = ({ navigation, route }) => {
    const headerOptions = {
        headerTitle: () => <HeaderTitle title="Pembelian Pulsa/Paket Data" />,
        headerRight: () => <HeaderButton accessibilityLabel="Bantuan" iconName="question-circle" onPress={handleHeaderHelpButton}/>,
    }

    useLayoutEffect(() => {
        navigation.setOptions(headerOptions)
    })

    const helps = getHelpString(route)

    const handleHeaderHelpButton = () => {
        navigation.navigate('Help', {
            helps: helps
        })
    }

    const balance = useRecoilValue(userBalanceState)

    return (
        <ScrollView>
            <View style={styles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <TabNavigator.Navigator backBehavior="none" >
                <TabNavigator.Screen name="Pulsa" component={PulsaBalanceScreen}/>
                <TabNavigator.Screen name="Paket" component={PacketDataScreen}/>
            </TabNavigator.Navigator>
        </ScrollView>            
    )
}

const styles = StyleSheet.create({
    header: {
        padding: 10,
        backgroundColor: 'white'
    }
})


export default PulsaScreen;