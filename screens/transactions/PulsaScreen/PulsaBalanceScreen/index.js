import React, { useState } from 'react'
import { screenStyles } from '../../styles'
import { View, Text } from 'react-native';
import TransactionNumberInput from '../../../../components/transaction/TransactionNumberInput';
import TransactionNumberHistory from '../../../../components/transaction/TransactionNumberHistory';
import TransactionOptionList from '../../../../components/transaction/TransactionOptionList';
import { useRecoilState } from 'recoil';
import { pulsaOptionsState } from '../../../../state/itemOption';
import { numberHistoryState } from '../../../../state/transaction';
import GridItemOptionMenu from '../../../../components/menu/GridItemOptionMenu';
import { validatePhoneNumber } from '../../../../utils/validation';
import { convertIntToIdrString } from '../../../../utils/currency';


const PulsaBalanceScreen = ({ navigation }) => {
    const [isCorrectNumber, setIsCorrectNumber] = useState(false)
    const [pulsaOptions, setPulsaOptions] = useRecoilState(pulsaOptionsState)
    const [numberHistory, setNumberHistory] = useRecoilState(numberHistoryState)
    const [phoneNumber, setPhoneNumber] = useState('')

    const renderPulsaOptionItem = ({ item }) => {
        const params = {
            transactionType: 'Pulsa',
            first: {
                fields: ["Nomor Ponsel", "Provider"],
                values: [phoneNumber, "Telkomsel"]
            },
            second: {
                fields: ["Voucher Pulsa", "Harga", "Biaya", "Total"],
                values: [item.title, ...[item.price, item.price/100, parseInt(item.price)+parseInt(item.price/100)].map((e) => convertIntToIdrString(e))]
            },
            success: {
                headerText: "Pengisian pulsa berhasil",
                subtitleText: `Untuk nomor ponsel ${phoneNumber}`,
                price: `${item.price}`
            }
        }
        return <GridItemOptionMenu item={item} onPress={() => navigation.navigate('Transaction', params)} />
    }

    const handleOnIconPress = () => {
        setPhoneNumber("087838888671")
    }

    return (
        <View style={screenStyles.container}>
            <TransactionNumberInput
                headerText="Nomor Ponsel" 
                iconName="square"
                iconOnPress={handleOnIconPress}
                number={phoneNumber}
                setNumber={setPhoneNumber}
                onCorrectInput={() => setIsCorrectNumber(true)}
                onFalseInput={() => setIsCorrectNumber(false)}
                validation={validatePhoneNumber}
            />
            {!isCorrectNumber ? 
                <TransactionNumberHistory 
                    numberHistory={numberHistory}
                    headerText="Nomor Sebelumnya" 
                    onNumberPress={setPhoneNumber}
                /> : 
                <TransactionOptionList
                    headerTitle="Pilih Voucher Pulsa"
                    headerSubtitle="Biaya transaksi Rp 100.000" 
                    optionData={pulsaOptions}
                    renderItem={renderPulsaOptionItem}
                    numColumns={2}
                />
            }
        </View>
    )
}

export default PulsaBalanceScreen;