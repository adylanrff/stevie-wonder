import React from 'react'
import { View, StyleSheet, Text, Button, Dimensions } from 'react-native'
import { useRecoilValue } from 'recoil'
import { userBalanceState } from '../../../state/auth'
import BalanceView from '../../../components/balance'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import TransactionDetailInfo from '../../../components/information/TransactionDetailInfo'

const TransactionDetailScreen = ({ route, navigation }) => {
    const balance = useRecoilValue(userBalanceState)

    const { first, second } = route.params;

    const handleConfirm = () => {
        if (Math.random() < 0.25) {
            navigation.navigate('TransactionFailed', {
                reason: 'Server down',
                prompt: "Silahkan kembali mencoba lagi beberapa saat"
            })
        } else {
            navigation.navigate('Confirmation', route.params)
        }
    }

    return (
        <ScrollView contentContainerStyle={styles.container}>
            <BalanceView text="Saldo E-Wallet" balance={balance}/>
            <TransactionDetailInfo first={first} second={second}/>
            <View style={styles.buttonSection}>
                <TouchableOpacity style={styles.button} onPress={() => navigation.goBack()}>
                    <Text style={styles.buttonText}>Ubah</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={handleConfirm}>
                    <Text style={styles.buttonText}>Konfirmasi</Text> 
                </TouchableOpacity>
            </View>


        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 30,
    },
    buttonSection: {
        marginTop: 24,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    button: {
        paddingVertical: 15,
        width: Dimensions.get('screen').width * 2/5,
        backgroundColor: 'white'
    },
    buttonText: {
        textAlign: 'center'
    }
})

export default TransactionDetailScreen;