import { StyleSheet } from "react-native"

export const screenStyles = StyleSheet.create({
    topUpContainer: {
        marginHorizontal: 32,
        marginVertical: 24,
        alignItems: 'stretch'
    },
    container: {
        marginHorizontal: 32,
        marginVertical: 64,
        alignItems: 'stretch'
    },
    header: {
        padding: 10,
        backgroundColor: 'white'
    },
    section: {
        display: 'flex',
        marginVertical: 25,
        alignItems: 'stretch',
    },
    topUpSection: {
        display: 'flex',
        marginVertical: 16,
        alignItems: 'stretch',
    },
    bottomButton: {
        marginVertical: 40,
    }
})
