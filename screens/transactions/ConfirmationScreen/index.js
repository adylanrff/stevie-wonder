import React from 'react';
import { StyleSheet } from 'react-native';
import PinForm from '../../../components/login/pin/PinForm';
import { KEYPAD_CONTENTS, MAX_PIN_LENGTH } from './constants'


const ConfirmationScreen = ({ navigation, route }) => {
  
  const handleOnSubmit = () => {
    navigation.navigate('TransactionSuccess', route.params)
  }

  const handleFingerprint = () => {
    navigation.navigate('Fingerprint', {
      onSubmit: handleOnSubmit
    })
  }

  return (
    <>
      <PinForm
        maxPinLength={MAX_PIN_LENGTH}
        keypadContents={KEYPAD_CONTENTS}
        onFingerprintPress={handleFingerprint}
        onSubmit={handleOnSubmit}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {

  }
})

export default ConfirmationScreen;