const MAX_PIN_LENGTH = 6;
const KEYPAD_CONTENTS = [
    {key: "1"},
    {key: "2"},
    {key: "3"},
    {key: "4"},
    {key: "5"},
    {key: "6"},
    {key: "7"},
    {key: "8"},
    {key: "9"},
    {key: "S"}, 
    {key: "0"}, 
    {key: "<<"}
]


export { KEYPAD_CONTENTS, MAX_PIN_LENGTH };