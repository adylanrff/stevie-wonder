import React, { useLayoutEffect } from 'react'
import { View, Text, Button, StyleSheet, Dimensions } from 'react-native';
import { screenStyles } from '../styles';
import BalanceView from '../../../components/balance';
import { useRecoilValue, useRecoilState } from 'recoil';
import { userBalanceState, userNameState, userPhoneNumberState} from '../../../state/auth';
import TransactionDetailInfo from '../../../components/information/TransactionDetailInfo';
import { otherTransactionState } from '../../../state/transaction';
import { convertIntToIdrString } from '../../../utils/currency';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import HeaderTitle from '../../../components/header/HeaderTitle';


const OtherScreen = ({ navigation }) => {
    const balance = useRecoilValue(userBalanceState)
    const username = useRecoilValue(userNameState)
    const userPhoneNumber = useRecoilValue(userPhoneNumberState)
    const [otherTransaction, setOtherTransaction] = useRecoilState(otherTransactionState)

    const headerOptions = {
        headerTitle: () => <HeaderTitle title="Pembayaran Lainnya" />,
    }

    useLayoutEffect(() => {
        navigation.setOptions(headerOptions)
    })

    const first = {
        fields: ["Nama Pelanggan", "Nomor Ponsel"],
        values: [username, userPhoneNumber],
    }

    const second = {
        fields: ["Nama Institutsi", "Nominal", "Biaya", "Total"],
        values: [otherTransaction.merchant, ...[otherTransaction.price, otherTransaction.price / 10,(otherTransaction.price + otherTransaction.price / 10) ].map(e => convertIntToIdrString(e)) ]
    }

    const handleBackPress = () => {
        navigation.goBack()
    }

    const handlePaymentPress = () => {
        navigation.navigate('Confirmation', {
            success: {
                headerText: "Pembayaran Sukses",
                subtitleText: `untuk ${otherTransaction.merchant}`,
                price: `${convertIntToIdrString(otherTransaction.price)}`,
            },
            first: first,
            second: second,
        })
    }

    return (
        <ScrollView>
            <View style={screenStyles.section}>
                <BalanceView text="Saldo E-Wallet" balance={balance} />
            </View>
            <View style={screenStyles.section}>
                <TransactionDetailInfo first={first} second={second} />
            </View>
            <View style={screenStyles.section}>
                <View style={styles.buttonOption}>
                    <TouchableOpacity style={styles.button} onPress={handleBackPress}>
                        <Text>Batal</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={handlePaymentPress}>
                        <Text>Konfirmasi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    buttonOption: {
        alignItems: 'stretch',
        flexDirection: 'row',
        alignItems: "center",
        alignSelf: 'center'
    },
    button: {
        paddingVertical: 15,
        margin: 10,
        width: Dimensions.get('screen').width * 2/5,
        backgroundColor: 'white',
        alignItems: 'center'
    },

    
})

export default OtherScreen;