import React from 'react'
import { Image, View, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import placeholderImg from '../../../assets/image/placeholder.jpg'

const TransactionSuccessScreen = ({ route, navigation }) => {

    const { success: { headerText, subtitleText, price } } = route.params 

    return (
        <View style={styles.container}>
            <Image source={placeholderImg} style={{width: 120, height: 120}} />

            <View style={styles.section}>    
                <Text style={styles.text}> { headerText } </Text>
                <Text style={styles.text}> { subtitleText }</Text> 
            </View>
            <View style={styles.section}>
                <Text style={styles.text}> Total Harga </Text> 
                <Text style={styles.text}> { price }</Text> 
            </View>

            <View style={styles.buttonSection}>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Transaction', route.params)}>
                    <Text>Lihat Detail</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
                    <Text>Menu Utama</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: { 
        flex: 1,
        margin: 96,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    text: {
        marginVertical: 10,
        fontSize: 16,
        textAlign: 'center'
    },
    section: {
        marginVertical: 20,
        alignItems: 'center'
    },
    buttonSection: {
        flexDirection: 'row',
    }, 
    button: {
        paddingVertical: 10,
        paddingHorizontal: 30,
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginVertical: 32,
    }
})

export default TransactionSuccessScreen;