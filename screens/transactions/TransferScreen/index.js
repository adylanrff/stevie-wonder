import React, { useState, useEffect, useLayoutEffect } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import BalanceView from '../../../components/balance';
import { screenStyles } from '../styles';
import TransactionNumberInput from '../../../components/transaction/TransactionNumberInput';
import { useRecoilValue } from 'recoil';
import { userBalanceState } from '../../../state/auth';
import { convertIntToIdrString } from '../../../utils/currency';
import { validatePhoneNumber } from '../../../utils/validation';
import HeaderTitle from '../../../components/header/HeaderTitle';
import HeaderButton from '../../../components/header/HeaderButton';
import { getHelpString } from '../../../utils/helpString';


const TransferScreen = ({ navigation, route }) => {
    const [isCorrectNumber, setIsCorrectNumber] = useState(false)
    const balance = useRecoilValue(userBalanceState)
    const [phoneNumber, setPhoneNumber] = useState('')
    const [nominal, setNominal] = useState('')
    const [canDoPayment, setCanDoPayment] = useState(false)

    const headerOptions = {
        headerTitle: () => <HeaderTitle title="Pembayaran Listrik PLN" />,
        headerRight: () => <HeaderButton accessibilityLabel="Bantuan" iconName="question-circle" onPress={handleHeaderHelpButton}/>,
    }

    useLayoutEffect(() => {
        navigation.setOptions(headerOptions)
    })

    const helpString = getHelpString(route)

    const handleHeaderHelpButton = () => {
        navigation.navigate('Help', {
            helps: helpString
        })
    }

    const handlePaymentPress = () => {
        const params = {
            transactionType: 'Transfer',
            first: {
                fields: ["Nomor Ponsel"],
                values: [phoneNumber]
            },
            second: {
                fields: ["Nominal Transfer", "Biaya", "Total"],
                values: [nominal, nominal/100, parseInt(nominal)+parseInt(nominal/100)].map((e) => convertIntToIdrString(e))
            },
            success: {
                headerText: "Transfer berhasil!",
                subtitleText: `ke pengguna ${phoneNumber}`,
                price: `${nominal}`
            }
        }

        navigation.navigate('Transaction', params)
    }

    const handleOnIconPress = () => {
        setPhoneNumber("087838888671")
    }

    useEffect(() => {
        console.log(parseInt(nominal))
        setCanDoPayment(parseInt(nominal) <= balance && isCorrectNumber)
    }, [balance, nominal, isCorrectNumber])

    return (
        <ScrollView>
            <View style={screenStyles.header}>
                <BalanceView text="Saldo E-Wallet" balance={balance}/>
            </View>
            <View style={screenStyles.container}>
                <View style={styles.textSection}>
                    <Text style={styles.headerText}>Transfer sesama E-Wallet</Text>
                    <Text style={styles.subtitleText}>Biaya transaksi 1.000.000</Text>
                </View>
                <TransactionNumberInput
                    headerText="Nomor Ponsel" 
                    iconName="square"
                    iconOnPress={handleOnIconPress}
                    number={phoneNumber}
                    setNumber={setPhoneNumber}
                    onCorrectInput={() => setIsCorrectNumber(true)}
                    onFalseInput={() => setIsCorrectNumber(false)}
                    validation={validatePhoneNumber}
                />
                <View style={screenStyles.section}>
                    <TransactionNumberInput
                        headerText="Nominal"
                        number={nominal}
                        setNumber={setNominal}
                        validation={() => nominal <= balance}
                    />
                </View>
                <View style={screenStyles.section}>
                    <Button
                        title="Lanjutkan Pembayaran"
                        disabled={!canDoPayment} 
                        onPress={handlePaymentPress}
                        color="black"
                    />
                </View>
            </View>

        </ScrollView>            
    )
}

const styles = StyleSheet.create({
    textSection: {
        alignItems: 'flex-start',
        marginBottom: 25,
    },
    headerText: {
        fontSize: 21,
        fontWeight: 'bold',
        marginVertical: 5,
    },
    subtitleText: {
        fontSize: 12,
    }
})

export default TransferScreen;